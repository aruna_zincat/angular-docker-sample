# Stage 1
FROM node:8 as builder

COPY angular-docker-sample /angular-docker-sample

WORKDIR /angular-docker-sample

RUN npm install
RUN $(npm bin)/ng build

# Stage 2 - the production environment
FROM nginx:alpine

COPY --from=builder /angular-docker-sample/dist/* /usr/share/nginx/html

EXPOSE 80